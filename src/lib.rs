use librust::{env, fs::metadata, process::Command};
use wasm_bindgen::prelude::*;

mod apply_fstab;
mod first_boot;

#[wasm_bindgen]
pub async fn start() {
    env::set_var("TERM", "wubix");
    bunt::println!("{$yellow+bold}Welcome to Wubix!{/$}").await;

    let is_first_boot = metadata("/dev/local_storage/bootloader").await.is_err();

    if is_first_boot {
        first_boot::first_boot().await;
    } else {
        apply_fstab::apply_fstab().await;
    }

    Command::new("/bin/sh")
        .env("PATH", "/bin:/usr/bin:/sbin:/usr/sbin")
        .status()
        .await
        .unwrap();

    futures::future::pending().await
}
