use librust::{
    fs,
    sys::{
        mount,
        types::system_calls::mount::{FileSystem, MountOptions},
    },
};

pub async fn apply_fstab() {
    let fstab = match fs::read_to_string("/etc/fstab").await {
        Ok(fstab) => fstab,
        Err(error) => {
            librust::eprintln!(
                "Error reading /etc/fstab: {}. The system will remain with initramfs. Now you're on your own.",
                error,
            ).await;
            return;
        }
    };

    for line in fstab.lines() {
        if line.starts_with('#') {
            continue;
        }

        let mut parts = line.split_whitespace();

        let device = match parts.next() {
            Some(device) => device,
            None => continue,
        };
        let path = match parts.next() {
            Some(path) => path,
            None => continue,
        };
        let fs = match parts.next() {
            Some(fs) => fs,
            None => continue,
        };
        let _options = match parts.next() {
            Some(options) => options.split(','),
            None => continue,
        };

        let file_system = match fs {
            "idbfs" => {
                let id = match device.strip_prefix("IDB_ID=") {
                    Some(id) => id,
                    None => continue,
                };

                FileSystem::Idbfs { id: id.to_owned() }
            }
            "devfs" => FileSystem::Devfs,
            "tmpfs" => FileSystem::Tmpfs,
            _ => continue,
        };

        let result = mount(
            path.as_bytes().to_owned(),
            MountOptions {
                file_system,
                should_remount: path == "/",
            },
        )
        .await;

        if let Err(error) = result {
            librust::eprintln!("Error mounting {}: {}", path, error).await;
        }
    }
}
