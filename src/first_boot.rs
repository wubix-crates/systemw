use librust::{
    fs::{copy, create_dir_all, write},
    io::{stdout, Write},
    path::Path,
    process::Command,
    sys::{
        mount,
        types::system_calls::mount::{FileSystem, MountOptions},
        unmount,
    },
};

const BASIC_DIRECTORIES: [&str; 16] = [
    "boot", "bin", "dev", "etc", "home", "mnt", "proc", "root", "sys", "tmp", "usr", "usr/bin",
    "var", "opt", "run", "srv",
];

const COPY_FILES: [(&str, &str); 1] = [("/bin/wpm", "/mnt/usr/bin/wpm")];

const BOOTLOADER_CONFIG: &str = r#"{
   "kernel": {
       "type": "idbfs",
       "id": "wubix",
       "path": "/boot/wubix"
   },
   "initramfs": {
       "type": "idbfs",
       "id": "wubix"
   }
}"#;

async fn prepare_root_file_system() {
    bunt::println!("Mounting an IndexedDB-based file system at {$cyan}/mnt{/$}... ").await;
    stdout().flush().await.unwrap();

    create_dir_all("/mnt").await.unwrap();
    mount(
        b"/mnt".to_vec(),
        MountOptions {
            file_system: FileSystem::Idbfs {
                id: "wubix".to_string(),
            },
            should_remount: false,
        },
    )
    .await
    .unwrap();

    for directory in &BASIC_DIRECTORIES {
        let path = Path::new("/mnt").join(directory);
        bunt::println!("Creating {$cyan}{}{/$}...", path.display()).await;
        create_dir_all(path).await.unwrap();
    }

    for &(from, to) in &COPY_FILES {
        bunt::println!("Copying {$cyan}{}{/$} to {$cyan}{}{/$}...", from, to).await;
        copy(from, to).await.unwrap();
    }

    bunt::println!("Creating {$cyan}/mnt/etc/fstab{/$}...").await;
    write("/mnt/etc/fstab", "IDB_ID=wubix / idbfs defaults")
        .await
        .unwrap();

    bunt::println!("Unmounting {$cyan}/mnt{/$}...").await;
    unmount(b"/mnt".to_vec()).await.unwrap();

    bunt::println!("Mounting the prepared file system as {$cyan}/{/$}...").await;
    mount(
        b"/".to_vec(),
        MountOptions {
            file_system: FileSystem::Idbfs {
                id: "wubix".to_string(),
            },
            should_remount: true,
        },
    )
    .await
    .unwrap();

    librust::println!("The root filesystem is ready for use!").await;
}

async fn install_core_packages() {
    bunt::println!("Installing core packages...").await;
    Command::new("/usr/bin/wpm")
        .args(&[
            "install",
            "wubix-kernel",
            "systemw",
            "wpm",
            "coreutils",
            "tinysh",
            "basic-ls",
            "--assume",
            "yes",
        ])
        .status()
        .await
        .unwrap();
}

pub async fn first_boot() {
    bunt::println!(
        "\nLooks like this is the first time you boot Wubix. {$green}systemw{/$} (Wubix's init \
         system) will prepare a persistent root filesystem and install core packages for you now."
    )
    .await;
    prepare_root_file_system().await;
    install_core_packages().await;

    write("/dev/local_storage/bootloader", BOOTLOADER_CONFIG)
        .await
        .unwrap();

    bunt::println!(
        "First boot preparation is done. You'll now be dropped into a simple shell, \
        {$green}tinysh{/$}, which, however, supports piping and redirection. A few core Unix \
        utilities, such as {$green}cat{/$}, {$green}echo{/$}, {$green}rm{/$} and other, \
        are available.\n\n\

        Some more programs, such as {$green}upload-file{/$}, which you can use to \
        upload files from your host computer to Wubix, and {$green}lolcat{/$}, are available \
        through {$green}wpm{/$}, Wubix Package Manager, e.g. {$#ffffff}wpm install upload-file{/$}. \
        In fact, {$green}coreutils{/$}, {$green}tinysh{/$} and {$green}wpm{/$} were just installed \
        with {$green}wpm{/$}. In case you want to update them, run {$#ffffff}wpm update{/$}.\n\n\

        And just so you know, Wubix is free software licensed under AGPL 3.0. The source code of \
        most important parts is available at {$#ffffff+underline}gitlab.com/SnejUgal/wubix{/$}, and \
        the source code of all ported crates is at {$#ffffff+underline}gitlab.com/wubix-crates{/$}. \
        {$dimmed}(You can't actually click those, type them in your browser.){/$}\n\n\

        {$yellow+bold}Enjoy Wubix!{/$}"
    )
    .await;
}
